import { listenAndServe } from "https://deno.land/std@0.61.0/http/server.ts";
import { acceptWebSocket, acceptable } from "https://deno.land/std@0.61.0/ws/mod.ts";
import { projects } from "./ws.ts";

// Server
listenAndServe({ port: 8000 }, async req => {
  if (req.method === "GET" && req.url === "/projects") {
    if (acceptable(req)) {
      acceptWebSocket({
        conn: req.conn,
        bufReader: req.r,
        bufWriter: req.w,
        headers: req.headers,
      }).then(projects);
    }
  }
});

console.log("Server running on localhost:8000");
