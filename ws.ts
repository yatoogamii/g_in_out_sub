import { WebSocket, isWebSocketCloseEvent } from "https://deno.land/std@0.61.0/ws/mod.ts";
import { v4 } from "https://deno.land/std@0.61.0/uuid/mod.ts";
import { Projects } from "./mongo.ts";

interface IListGroupProjects {
  data: any;
  listeners: WebSocket[];
}

const listGroupeProjets = new Map<string, IListGroupProjects>();

export async function projects(ws: WebSocket): Promise<void> {
  try {
    let groupProjectId: string;
    let groupProjectData;

    for await (const event of ws) {
      if (typeof event === "string" && event !== "") {
        const method = JSON.parse(event).method;
        groupProjectId = JSON.parse(event).groupProjectId;

        if (method === "get") {
          groupProjectData = await Projects.findOne({ _id: { $oid: groupProjectId } });

          if (listGroupeProjets.has(groupProjectId)) {
            // add user to groupProjectId listener
            const listeners = listGroupeProjets.get(groupProjectId)?.listeners ?? [];
            listGroupeProjets.set(groupProjectId, { data: groupProjectData, listeners: [...listeners, ws] });
          } else {
            // create key with projectId and add user to listener
            listGroupeProjets.set(groupProjectId, { data: groupProjectData, listeners: [ws] });
          }
        } else if (method === "update") {
          const listeners = listGroupeProjets.get(groupProjectId)?.listeners ?? [];
          const newProjectData = JSON.parse(event)?.projectData ?? {};
          listGroupeProjets.set(groupProjectId, {
            data: { ...groupProjectData, ...newProjectData },
            listeners: [...listeners],
          });
          await Projects.updateOne(
            { _id: { $oid: groupProjectId } },
            { $set: { ...listGroupeProjets.get(groupProjectId)?.data } },
          );

          listeners.forEach(ws => {
            ws.send(JSON.stringify({ groupProjectId, projectData: listGroupeProjets.get(groupProjectId)?.data }));
          });
        }

        ws.send(JSON.stringify({ groupProjectId, projectData: listGroupeProjets.get(groupProjectId)?.data }));
      }

      if (isWebSocketCloseEvent(event)) {
        for (const project of listGroupeProjets.values()) {
          const data = project.data ?? {};
          let listeners = project.listeners ?? [];

          listeners = listeners.filter(e => e !== ws);

          if (listeners.length > 0) {
            // update groupProjectId listener without ws
            listGroupeProjets.set(data._id, { data: groupProjectData, listeners: listeners });
          } else {
            // delete groupProjectId of map
            listGroupeProjets.delete(data._id);
          }
        }

        break;
      }
    }
  } catch (e) {
    console.log(e);
  }
}
