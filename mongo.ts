import { MongoClient } from "https://deno.land/x/mongo@v0.9.1/mod.ts";

// Mongo
const client = new MongoClient();
client.connectWithUri("mongodb://127.0.0.1:27017");
const db = client.database("ginga");

interface Project {
  _id: { $oid: string };
  name: string;
}

export const Projects = db.collection<Project>("projects");
